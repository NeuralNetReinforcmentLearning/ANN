package ch.flickersoft.ann;

import java.util.ArrayList;
import java.util.Random;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class Network {
	RealVector[] zs;//z->N->a
	RealVector[] as;
	RealVector[] biases;
	RealMatrix[] weights;
	int[] layerSizes;
	int layers;
	Random r=new Random();
	
	GraphDisplay costGraph;
	ArrayList<Double>costHistory=new ArrayList<>();
	
	public Network(int[] layerSizes) {
		this.layerSizes=layerSizes;
		layers=layerSizes.length;
		biases=new ArrayRealVector[layers-1];
		weights=new Array2DRowRealMatrix[layers-1];
		zs=new ArrayRealVector[layers-1];
		as=new ArrayRealVector[layers];
		
		//Random initialisation
		for(int i=0; i<layers-1; i++){
			double[] bRand=new double[layerSizes[i+1]];
			for(int j=0; j<bRand.length; j++)bRand[j]=r.nextDouble()*2-1;
			biases[i]=new ArrayRealVector(bRand);
			
			double[][] mRand=new double[layerSizes[i]][layerSizes[i+1]];
			for(int j=0; j<mRand.length; j++){
				for(int k=0; k<mRand[j].length; k++){
					mRand[j][k]=r.nextDouble()*2-1;
				}
			}
			weights[i]=new Array2DRowRealMatrix(mRand);
		}
		
		costGraph=new GraphDisplay(new double[][]{new double[]{0}}, 60000, 0, 1);
	}
	
	RealVector feedForward(RealVector input){
		as[0]=input;
		for(int i=0; i<layers-1; i++){
			zs[i]=weights[i].preMultiply(as[i]).add(biases[i]);
			as[i+1]=activation(zs[i]);
		}
		return as[layers-1];
	}
	
	RealVector feedForward(double... inputs){
		return feedForward(new ArrayRealVector(inputs));
	}
	
	/*RealVector feedForward(double action, double[] stateinfo){
		double[] inputs=new double[layerSizes[0]];
		for(int i=0; i<inputs.length; i++){
			if(i==action)inputs[i]=1;
			else if(i>=inputs.length-stateinfo.length)inputs[i]=stateinfo[i+stateinfo.length-inputs.length];
			else inputs[i]=0;
		}
		return feedForward(new ArrayRealVector(inputs));
	}*/
	
	RealVector feedForward(double action, double[] stateinfo){
		double[] inputs=new double[stateinfo.length+1];
		for(int i=0; i<inputs.length; i++){
			if(i==0)inputs[i]=action;
			else inputs[i]=stateinfo[i-1];
		}
		return feedForward(new ArrayRealVector(inputs));
	}
	
	//Rly elegant indeed
	class BackPropagationOutput{
		RealVector[] nablaBiases;
		RealMatrix[] nablaWeights;
		public BackPropagationOutput(RealVector[] nablaBiases, RealMatrix[] nablaWeights) {
			this.nablaBiases=nablaBiases;
			this.nablaWeights=nablaWeights;
		}
	}
	
	/*double cost(RealVector answer){
		double cost=0;
		//RealVector diff=answer.subtract(as[layers-1]);
		for(int i=0; i<as[layers-1].getDimension(); i++)cost+=(answer.getEntry(i)*Math.log(as[layers-1].getEntry(i))+(1-answer.getEntry(i))*Math.log(1-as[layers-1].getEntry(i)));
		cost*=-1;
		return cost;
	}*/
	
	double cost(RealVector answer){
		double cost=0;
		RealVector diff=answer.subtract(as[layers-1]);
		for(int i=0; i<diff.getDimension(); i++)cost+=Math.pow(diff.getEntry(i),2);
		cost=cost/2;
		return cost;
	}
	
	BackPropagationOutput backpropagate(RealVector answer){
		RealVector[] nablaBiases=new ArrayRealVector[layers-1];
		RealMatrix[] nablaWeights=new Array2DRowRealMatrix[layers-1];
		RealVector[] errors=new ArrayRealVector[layers-1];
		
		/*double[] ng=new double[(int) costGraph.xMax];
		costHistory.add(cost(answer));
		if(costHistory.size()>ng.length)costHistory.remove(0);
		
		int avg=50;
		
		for(int i=0; i<costHistory.size(); i++){
			if(i<avg)ng[i]=costHistory.get(i);
			else{
				double sum=0;
				for(int j=-avg; j<0; j++)sum+=ng[i+j];
				sum+=costHistory.get(i);
				sum=sum/(avg+1);
				ng[i]=sum;
			}
		}*/
		/*for(int i=0; i<ng.length; i++){
			if(i<ng.length-1)ng[i]=costGraph.graphs[0][i];
			else ng[i]=cost(answer);
		}*/
		//costGraph.graphs[0]=ng;
		//costGraph.changexMax(costGraph.xMax+1);
		//costGraph.repaint();
		//System.out.println(cost(answer));
		
		RealVector outputError=as[layers-1].subtract(answer);
		//outputError=hadamardProduct(outputError, outputError);
		errors[errors.length-1]=outputError;
		for(int i=layers-2; i>=0; i--){
			//Calculating Bias derivatives
			nablaBiases[i]=errors[i];
			
			//Calculating Weight derivatives
			double[][] tWeights=new double[as[i].getDimension()][errors[i].getDimension()];
			for(int j=0; j<as[i].getDimension(); j++){
				for(int k=0; k<errors[i].getDimension(); k++){
					tWeights[j][k]=as[i].getEntry(j)*errors[i].getEntry(k);
				}
			}
			nablaWeights[i]=new Array2DRowRealMatrix(tWeights);
			
			//Packpropagating errors further
			if(i>0){
				//errors[i-1]=weights[i].transpose().preMultiply(errors[i]);
				errors[i-1]=hadamardProduct(weights[i].transpose().preMultiply(errors[i]),activationPrime(zs[i-1]));
			}
		}
		return new BackPropagationOutput(nablaBiases, nablaWeights);
	}
	
//	BackPropagationOutput backpropagate(RealVector answer){
//		RealVector[] nablaBiases=new ArrayRealVector[layers-1];
//		RealMatrix[] nablaWeights=new Array2DRowRealMatrix[layers-1];
//		RealVector[] errors=new ArrayRealVector[layers-1];
//		
//		double[] ng=new double[(int) costGraph.xMax];
//		costHistory.add(cost(answer));
//		if(costHistory.size()>ng.length)costHistory.remove(0);
//		
//		int avg=50;
//		
//		for(int i=0; i<costHistory.size(); i++){
//			if(i<avg)ng[i]=costHistory.get(i);
//			else{
//				double sum=0;
//				for(int j=-avg; j<0; j++)sum+=ng[i+j];
//				sum+=costHistory.get(i);
//				sum=sum/(avg+1);
//				ng[i]=sum;
//			}
//		}
//		/*for(int i=0; i<ng.length; i++){
//			if(i<ng.length-1)ng[i]=costGraph.graphs[0][i];
//			else ng[i]=cost(answer);
//		}*/
//		costGraph.graphs[0]=ng;
//		//costGraph.changexMax(costGraph.xMax+1);
//		costGraph.repaint();
//		//System.out.println(cost(answer));
//		
//		RealVector outputError=hadamardProduct(as[layers-1].subtract(answer),activationPrime(zs[zs.length-1]));
//		errors[errors.length-1]=outputError;
//		for(int i=layers-2; i>=0; i--){
//			//Calculating Bias derivatives
//			nablaBiases[i]=errors[i];
//			
//			//Calculating Weight derivatives
//			double[][] tWeights=new double[as[i].getDimension()][errors[i].getDimension()];
//			for(int j=0; j<as[i].getDimension(); j++){
//				for(int k=0; k<errors[i].getDimension(); k++){
//					tWeights[j][k]=as[i].getEntry(j)*errors[i].getEntry(k);
//				}
//			}
//			nablaWeights[i]=new Array2DRowRealMatrix(tWeights);
//			
//			//Packpropagating errors further
//			if(i>0){
//				errors[i-1]=hadamardProduct(weights[i].transpose().preMultiply(errors[i]),activationPrime(zs[i-1]));
//			}
//		}
//		return new BackPropagationOutput(nablaBiases, nablaWeights);
//	}
	
	void gradDescent(double learningRate, BackPropagationOutput gradients){
		for(int i=0; i<layers-1; i++){
			for(int j=0; j<layerSizes[i+1]; j++){
				double newVal=biases[i].getEntry(j)-learningRate*gradients.nablaBiases[i].getEntry(j);
				biases[i].setEntry(j, newVal);
			}
			for(int j=0; j<layerSizes[i]; j++){
				for(int k=0; k<layerSizes[i+1]; k++){
					double newVal=weights[i].getEntry(j,k)-learningRate*gradients.nablaWeights[i].getEntry(j,k);
					weights[i].setEntry(j, k, newVal);
				}
			}
		}
	}
	
	double activation(double z){
		return 1/(1+Math.exp(-z));
	}
	
	/*double activation(double z){
		if(z>40)return z;
		else if(z<-40)return 0;
		else return Math.log(1+Math.exp(z));
		//return Math.log(1+Math.exp(z)); Java cant handle
	}*/
	
	RealVector activation(RealVector z){
		double[] os=new double[z.getDimension()];
		for(int i=0; i<os.length; i++)os[i]=activation(z.getEntry(i));
		return new ArrayRealVector(os);
	}
	
	double activationPrime(double z){
		if(z>40)return 0;
		else if(z<-40)return 0;
		else {
			double ehz=Math.exp(-z);
			return ehz/Math.pow((1+ehz),2);
		}
	}
	
	/*double activationPrime(double z){
		//return 1/(1+Math.exp(-z));
		if(z>40)return 1;
		else if(z<-40)return 0.01;
		else return Math.max(1/(1+Math.exp(-z)),0.01);
	}*/
	
	RealVector activationPrime(RealVector z){
		double[] os=new double[z.getDimension()];
		for(int i=0; i<os.length; i++)os[i]=activationPrime(z.getEntry(i));
		return new ArrayRealVector(os);
	}
	
	static RealVector hadamardProduct(RealVector v1, RealVector v2){
		if(v1.getDimension()!=v2.getDimension())throw new IllegalArgumentException();
		double[] os=new double[v1.getDimension()];
		for(int i=0; i<v1.getDimension(); i++)os[i]=v1.getEntry(i)*v2.getEntry(i);
		return new ArrayRealVector(os);
	}
}
