package ch.flickersoft.ann;

import java.io.File;
import java.nio.file.Files;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import ch.flickersoft.ann.Network.BackPropagationOutput;



public class Main {
	
	static Network net;
	static int[] labels;
	static int[][] images;
	
	public static void main(String[] args) throws Exception {
		net=new Network(new int[]{1,5,1});
		
		/*loadTrainingImages();
		System.out.println(labels.length+" Images loaded");
		*/
		int corr=0;
		int runs=100000;
		int steps=0;
		for(int e=0; e<5; e++){
			for(int i=0; i<runs; i++){
				steps++;
				double a=Math.random()*2*Math.PI;
				double ans=Math.abs(Math.sin(a));
				RealVector out=net.feedForward(new ArrayRealVector(new double[]{a}));
				if(Math.round(out.getEntry(0)*100)==Math.round(ans*100))corr++;
				//corr+=(Math.abs(out-ans));
				net.gradDescent(1, net.backpropagate(new ArrayRealVector(new double[]{Math.abs(ans)})));
				System.out.println(out.getEntry(0)+":"+ans);
			}
		}
		//System.out.println(corr);
		System.out.println(corr/(double)steps);
		//System.out.println(net.weights[0]);
	}
	
	static double[] getInput(int idx){
		double[] out=new double[images[idx].length];
		for(int i=0; i<out.length; i++)out[i]=images[idx][i]/255d;
		return out;
	}
	
	static double[] getOutput(int idx){
		double[] out=new double[10];
		out[labels[idx]]=1;
		return out;
	}
	
	static void checkGradient(){
		double eps=0.0001;
		
		double biasval=net.biases[0].getEntry(0);
		
		net.biases[0].setEntry(0, biasval+eps);
		net.feedForward(new ArrayRealVector(new double[]{10}));
		double yp=net.cost(new ArrayRealVector(new double[]{0.5}));
		
		net.biases[0].setEntry(0, biasval-eps);
		net.feedForward(new ArrayRealVector(new double[]{10}));
		double ym=net.cost(new ArrayRealVector(new double[]{0.5}));
		
		net.biases[0].setEntry(0, biasval);
		net.feedForward(new ArrayRealVector(new double[]{10}));
		
		System.out.println(yp);
		System.out.println(ym);
		
		double eGrad=(yp-ym)/(2*eps);
		
		BackPropagationOutput grads=net.backpropagate(new ArrayRealVector(new double[]{0.5}));
		System.out.println(eGrad);
		System.out.println(grads.nablaBiases[0].getEntry(0));
	}
	
	static void loadTrainingImages() throws Exception{
		File labelFile=new File("A:\\Schule\\Maturarbeit\\train-labels.idx1-ubyte");
		byte[] lblContent=Files.readAllBytes(labelFile.toPath());
		labels=new int[lblContent.length-8];
		for(int i=8; i<lblContent.length; i++){
			labels[i-8]=Byte.toUnsignedInt(lblContent[i]);
		}
		File picFile=new File("A:\\Schule\\Maturarbeit\\train-images.idx3-ubyte");
		byte[] picContent=Files.readAllBytes(picFile.toPath());
		//BufferedImage[] pics=new BufferedImage[labels.length];
		images=new int[60000][784];
		int j=0;
		for(int i=16; i<picContent.length; i++){
			images[(i-16)/(784)][j]=Byte.toUnsignedInt(picContent[i]);
			j++;
			if(j>783)j=0;//784=28*28
		}
	}
}
